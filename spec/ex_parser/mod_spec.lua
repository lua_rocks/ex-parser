local assert = require("luassert")
local D = require 'dprint'
local M = require("ex_parser.grammar");

local mixfile = [==[
# comment 0
# comment 1
defmodule TaskDesk.MixProject do
  use Mix.Project

  def project do
    [
      app: :taskdesk,
      version: "0.1.0",
      elixir: "~> 1.16",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {TaskDesk.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ecto, "~> 3.0"},
      # {:siblings, "~> 0.1"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end
end
# comment eof
]==]


-- it("defmodule", function()
--   local code = [[
--   defmodule Desk.MixProject do
--     def project do
--     end
--   end
--   ]]
--   -- D.enable()
--   -- local res = M.parse(code, { debug = true })
--   -- assert.same(1, res)
-- end)

local mix_modname = "TaskDesk.MixProject"

local mix_fn_application = {
  tag = 'Function',
  name = "application",
  code = {
    tag = 'Code',
    value = {
      {
        tag = "List",
        value = {
          { -- el 1
            tag = "Tuple",
            value = {
              ":extra_applications",
              { tag = "List", value = { ":logger" } }
            }
          },
          { -- el 2
            tag = "Tuple",
            value = {
              ":mod",
              {
                tag = "Tuple",
                value = {
                  "TaskDesk.Application",
                  { tag = "List", value = {} }
                }
              }
            }
          }
        }
      } -- list return value from function
    }
  }
}

local mix_fn_deps = {
  tag = "Function",
  name = "deps",
  private = true,
  code = {
    tag = 'Code',
    value = {
      {
        tag = "List",
        value = {
          { tag = "Tuple",   value = { ":ecto", "~> 3.0" } },
          { tag = "Comment", '# {:siblings, "~> 0.1"}' },
          { tag = "Comment", '# {:dep_from_hexpm, "~> 0.3.0"},' },
          {
            tag = "Comment",
            '# {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}',
          }
        }
      }
    }
  }
}

local mix_fn_project = {
  tag = "Function",
  name = "project",
  code = {
    tag = 'Code',
    value = {
      {
        tag = "List",
        value = {
          { -- el 1
            tag = "Tuple", value = { ":app", ":taskdesk" }
          },
          { -- el 2
            tag = "Tuple", value = { ":version", "0.1.0" }
          },
          { -- el 3
            tag = "Tuple", value = { ":elixir", "~> 1.16" }
          },
          { -- el 4
            tag = "Tuple",
            value = {
              ":start_permanent",
              {
                tag = "ExprBin",
                value = {
                  left = {
                    tag = "Call",
                    value = { module = "Mix", func = "env", params = {} }
                  },
                  op = "==",
                  right = ":prod",
                }
              }
            }
          },
          { -- el 5
            tag = "Tuple",
            value = {
              ":deps", { tag = "Call", value = { func = "deps", params = {} } }
            }
          }
        }
      }
    }
  }
}

local mix_ast = {
  tag = "SourceFile",
  comments = {
    "# comment 0",
    "# comment 1",
    "# comment eof",
  },
  modules = {
    ["TaskDesk.MixProject"] = {
      tag = "Module",
      name = "TaskDesk.MixProject",
      use = { ["Mix.Project"] = true },

      functions = {
        application = mix_fn_application,
        deps = mix_fn_deps,
        project = mix_fn_project,
      }
    }
  }
}

describe("parsing mix.exs into AST", function()
  before_each(function()
    D.disable()
    mix_ast.modules.Mix = nil --
  end)

  it("parse", function()
    -- D.enable(true)
    local exp = mix_ast
    local res = M.parse(mixfile, { debug = false })
    -- print("local exp = ", require "inspect" (res))
    -- local mod = "TaskDesk.MixProject"
    -- assert.same(exp.modules[mod], res.modules[mod])
    assert.same(exp, res)
  end)


  it("ast2value List skip comments", function()
    local ast_deps = {
      tag = "List",
      value = {
        { tag = "Tuple",   value = { ":a", "~> 3.0" } },
        { tag = "Comment", '# {:siblings, "~> 0.1"}' },
        { tag = "Comment", '# {:dep_from_hexpm, "~> 0.3.0"},' },
        { tag = "Comment", '# {:dep_from_git, git: "https://github.com/.."}' },
        { tag = "Tuple",   value = { ":b", "~> 1.0" } },
      }
    }
    local exp = { { ':a', '~> 3.0' }, { ':b', '~> 1.0' } }
    assert.same(exp, M.ast2value(ast_deps))
  end)

  it("get_function_sub_ast", function()
    local f = M.get_function_sub_ast
    local ast = mix_ast
    local exp = mix_fn_deps
    assert.same(exp, f(ast, mix_modname, 'deps'))
    assert.same(mix_fn_project, f(ast, mix_modname, 'project'))
    assert.same(mix_fn_application, f(ast, mix_modname, 'application'))
    -- fail
    assert.match_error(function()
      f(ast, mix_modname, 'nonexisted')
    end, 'function TaskDesk.MixProject.nonexisted') -- not found
  end)

  local function mk_ctx(root, modname)
    local ctx = {
      root = root or mix_ast,
      modulename = modname or mix_modname,
      interpreter = M.simple_interpreter
    }
    --ctx = M.addMixStub(ctx)
    return ctx
  end

  it("mk_ctx", function()
    local ctx = mk_ctx()
    assert.same({
      root = mix_ast,
      modulename = mix_modname,
      interpreter = M.simple_interpreter
    }, ctx)
    assert.no_error(function() M.validate_ctx(ctx) end)
  end)

  it("Call private function deps()", function()
    local f = M.simple_interpreter
    local astnode = {
      tag = "Call", value = { func = "deps", params = {} }
    }
    local exp = { { ":ecto", "~> 3.0" } } -- list of one tuple

    assert.same(exp, f(astnode, mk_ctx()))
  end)


  it("to obj deps()", function()
    local f = M.ast2value
    local tuple_ast = mix_fn_project.code.value[1].value[5]

    local exp_tuple_ast = {
      tag = "Tuple",
      value = { ":deps",
        { tag = "Call", value = { func = "deps", params = {} } }
      }
    }
    assert.same(exp_tuple_ast, tuple_ast)

    local exp = { ':deps', { { ':ecto', '~> 3.0' } } }
    assert.same(exp, f(tuple_ast, mk_ctx()))
  end)

  -- goal ability to call Mix.env()
  it("addMixStub", function()
    local ctx = mk_ctx()
    ctx = M.addMixStub(ctx) -- emulate for Mix.env()
    assert.is_not_nil(M.get_function_sub_ast(ctx.root, 'Mix', 'env'))
    local ast = {
      tag = "Call",
      value = { func = "env", module = "Mix", params = {} }
    }
    local res = M.ast2value(ast, ctx)
    assert.same(':false', res)
  end)


  it("mixfile to_obj", function()
    local exp = {
      ["TaskDesk.MixProject"] = {
        application = {
          { ":extra_applications", { ":logger" } },
          { ":mod",                { "TaskDesk.Application", {} } }
        },
        project = {
          { ":app",             ":taskdesk" },
          { ":version",         "0.1.0" },
          { ":elixir",          "~> 1.16" },
          { ":start_permanent", false },                    -- evaluated
          { ":deps",            { { ":ecto", "~> 3.0" } } } -- evaluated
        }
        -- deps = { { ":ecto", "~> 3.0" } }, -- private
      }
    }
    local ast = M.parse(mixfile)
    local res = M.to_obj(ast, M.addMixStub(mk_ctx()))
    assert.same(exp, res)
  end)

  --[[
  it("mixfile to_obj", function()
    local project = {
      { ":app",     ":taskdesk" },
      { ":version", "0.1.0" }, { ":elixir", "~> 1.16" },
      { ":start_permanent", -- evaluate to false
        {
          tag = "ExprBin",
          value = {
            left = {
              tag = "Call",
              value = { func = "env", module = "Mix", params = {} }
            },
            op = "==",
            right = ":prod",
          }
        }
      },
      { ":deps", { { ":ecto", "~> 3.0" } } } -- evaluated
    }
  end)
]]

  it("parse 2 bag with empty list with comments", function()
    local mixfile2 = [==[defmodule Some.MixProject do
  use Mix.Project

  def project do
    [
      app: :my_app,
      version: "0.1.0",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      "remove this and there will be an error",
      # {:dep_from_hexpm, "~> 0.3.0"},
    ]
  end
end]==]

    local exp = {
      ["Some.MixProject"] = {
        application = {
          { ":extra_applications", { ":logger" } }
        },
        project = {
          { ":app",             ":my_app" },
          { ":version",         "0.1.0" },
          { ":start_permanent", false },
          { ":deps",            { "remove this and there will be an error" } }
        }
      },
      Mix = { env = ":false" }, -- Stub
    }
    local ast = M.parse(mixfile2)
    assert.is_not_nil(ast)

    local res = M.to_obj(ast, M.mk_ctx(ast, true))
    assert.same(exp, res)
  end)
end)
