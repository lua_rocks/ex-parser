local assert = require("luassert")

local M_debug = false

local I = function(tag)
  return lpeg.P(function(s, i)
    if M_debug then
      print(tag .. ' pos:' .. tostring(i) .. ' sub:"' .. s:sub(i, i) .. '"')
    end
    return true
  end)
end

describe("learn lpeg", function()
  local lpeg = require 'lpeg'
  local loc = lpeg.locale()


  it("how to combine lpeg patterns", function()
    local pDot = lpeg.P(".")
    local pUnderscore = lpeg.P("_")
    local p = (lpeg.R("az") + pDot + pUnderscore) ^ 1

    assert.same(8, p:match("abc_d.e"))
    local p2 = lpeg.C(p)
    assert.same('abc_d.e', p2:match("abc_d.e"))

    assert.same('abc_d.e', p2:match("abc_d.e "))
    assert.same('abc_d.e', p2:match("abc_d.e, "))
    assert.is_nil(p2:match(" abc_d.e, "))
  end)


  it("fold + Ct", function()
    S = loc.space ^ 0           -- spaces
    local num = (loc.digit ^ 1 / tonumber) * S
    local opA = lpeg.C("+") * S -- add operator
    local opS = lpeg.C("-") * S -- sub operator
    local exp = lpeg.Ct(S * num * ((opA + opS) * num) ^ 0)
    local t = exp:match("34 + 89 - 23")
    assert.same({ 34, '+', 89, '-', 23 }, t)

    local function foldBin(lst)
      local acc = lst[1]
      for i = 2, #lst, 2 do
        if lst[i] == "+" then
          acc = acc + lst[i + 1]
        else -- must be "-"
          acc = acc - lst[i + 1]
        end
      end
      return acc
    end

    assert.same(100, foldBin(t))

    -- mk function-capture
    local exp2 = lpeg.Ct(S * num * ((opA + opS) * num) ^ 0) / foldBin
    assert.same(100, exp2:match("34 + 89 - 23"))
  end)

  it("tuple", function()
    local S = loc.space ^ 0
    local p = lpeg.P("{") * S * (lpeg.C(loc.digit) / tonumber) * S * lpeg.P("}")
    local f = function(input) return lpeg.match(p, input) end

    assert.same(nil, f('{}'))
    assert.same(1, f('{1}'))
    assert.same(1, f('{ 1 }'))
  end)

  it("collect values to table", function()
    -- C(V("value")) ^ 0 * S * I('!<Tuple') * pCCB) * I('#<tuple')
    local C, Ct = lpeg.C, lpeg.Ct

    local p = Ct(C(lpeg.P('a')))
    assert.same({ 'a' }, lpeg.match(p, 'aa'))

    local p2 = Ct(C(lpeg.P('a')) ^ 0)
    assert.same({ 'a', 'a', 'a' }, lpeg.match(p2, 'aaa'))

    local p3 = Ct(C(lpeg.P('a') ^ 0))
    assert.same({ 'aaa' }, lpeg.match(p3, 'aaa'))
  end)

  it("lpeg.B before", function()
    ---@diagnostic disable-next-line: unused-local
    local B, P, S, C = lpeg.B, lpeg.P, lpeg.S, lpeg.C
    local spaces = loc.space ^ 0
    -- local p = B(1 - P(':')) * ':' * loc.alpha * (loc.alnum + S('_@')) ^ 0 * S('?!') ^ -1
    local p = B(1 - P(':')) * ':' * loc.alpha * (loc.alnum + S('_@')) ^ 0 * S('?!') ^ -1
    p = lpeg.C(p)
    local p2 = spaces * P('a') * p
    assert.same(':bcd?', lpeg.match(p2, ' a:bcd?'))

    -- it doesn't work because for lpeg.B it is necessary that
    -- there are characters before the current position and not the beginning
    -- of the string
    assert.is_nil(p:match(':b'))
    assert.same(':b', (spaces * p):match(' :b'))
    assert.same(':b', (spaces * p):match('  :b'))

    local p3 = spaces * P(':') * p
    assert.is_nil(lpeg.match(p3, ' ::not_an_atom'))
    --                             ^ this not passed by lpeg.B(1-P(':'))

    local p4 = spaces * P(':') * spaces * p
    assert.same(':atom', lpeg.match(p4, ' : :atom'))

    -- ^
    assert.same(1, lpeg.match(lpeg.P(-1), ''))

    local p5 = lpeg.P(-1) * ':' * loc.alpha
    assert.is_nil(lpeg.match(p5, ':a'))

    local p6 = (lpeg.P(-1) + B(1 - P(':'))) * ':' * loc.alpha
    assert.is_nil(lpeg.match(p6, ':a'))
  end)

  it("atom1", function()
    local P = lpeg.P
    local p = P('"') * (P(1) - P('"')) ^ 0 * P('"')
    assert.same(3, lpeg.match(p, '""'))
    assert.same(4, lpeg.match(p, '"a"'))
    assert.same(6, lpeg.match(p, '"abc"'))
    assert.same(7, lpeg.match(p, '"ab c"'))
    assert.same(4, lpeg.match(p, '"a\" c"')) -- !

    local p2 = P(':"') * (P(1) - P('"')) ^ 0 * P('"')
    assert.same(4, lpeg.match(p2, ':""'))
  end)

  it("heredoc", function()
    -- local I = M.I
    local ESC = '\\'
    local S = loc.space ^ 0
    local pNewline = lpeg.P('\r') ^ -1 * '\n'
    local escapes_t = { n = "\n", t = "\t", ['"'] = '"', [ESC] = ESC }
    local pEscapes = lpeg.S('nt') + ESC + '"'
    local pSContentM = (-lpeg.P('\n"""') * (lpeg.P(1) - ESC) + ESC * lpeg.C(pEscapes) / escapes_t) ^ 0
    assert.same('\\', lpeg.match(pSContentM, '\\\\'))
    assert.same("\n", lpeg.match(pSContentM, '\\n'))
    assert.same(2, lpeg.match(pSContentM, '\n'))
    assert.same(4, lpeg.match(pSContentM, 'abc'))
    assert.same(1, lpeg.match(pSContentM, ''))

    assert.same(2, lpeg.match(pNewline, "\n"))
    assert.is_nil(lpeg.match(pNewline, "\\n"))

    local pStringM = S * I('#1') * '"""' * I('#2') * lpeg.C(pSContentM) * I('#3') * pNewline * '"""' * I('#OK')
    assert.is_nil(lpeg.match(pStringM, '""""""'))
    assert.same('', lpeg.match(pStringM, '"""\n"""'))
    assert.same(' """', lpeg.match(pStringM, '""" """\n"""'))
    assert.same(' """' .. "\n\n" .. ' """ ', lpeg.match(pStringM, '""" """\n\n """ \n"""'))
  end)
end)
