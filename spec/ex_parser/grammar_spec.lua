local assert = require("luassert")
local inspect = require("inspect")
local D = require 'dprint'
local M = require("ex_parser.grammar");

local TUPLE = M.tags.tuple
local LIST = M.tags.list
local COMMENT = M.tags.comment
local TAG_VALUE = M.tags.value

-----------------------------------------------------------------------------

describe("ex_parser.grammar astnode to value", function()
  it("assert_tag", function()
    local f = M.assert_tag
    assert.match_error(function() f(nil, TUPLE) end,
      'expected table got: nil')

    assert.match_error(function() f({}, TUPLE) end,
      'expected tag == Tuple got: nil')

    assert.match_error(function() f({ tag = LIST }, TUPLE) end,
      'expected tag == Tuple got: List')

    assert.no_error(function() f({ tag = TUPLE }, TUPLE) end)
  end)

  it("ast2value", function()
    local f = M.ast2value
    assert.same({}, f({ tag = 'Tuple', value = {} }))
    assert.same({ 1, 2, 3 }, f({ tag = 'Tuple', value = { 1, 2, 3 } }))
    assert.same({ 1, 2, 3 }, f({ tag = 'Tuple', value = { 1, 2, 3 } }))

    -- ex: {}
    assert.same({}, f({ tag = M.tags.tuple, value = {} }))

    -- ex: {{}}
    local astnode = {
      tag = M.tags.tuple,
      value = { { tag = M.tags.tuple, value = {} } }
    }
    assert.same({ {} }, f(astnode))

    -- ex: {:ok, {}}
    local astnode2 = {
      tag = M.tags.tuple,
      value = { ':ok', { tag = M.tags.tuple, value = {} } }
    }
    assert.same({ ':ok', {} }, f(astnode2))

    --  ex: {:ok, P{}
    local astnode3 = {
      tag = M.tags.tuple,
      value = { ':ok',
        {
          tag = M.tags.tuple,
          value = {
            { tag = M.tags.tuple, value = { 1 } } -- 1 in tuple
          }
        }
      }
    }
    assert.same({ ':ok', { { 1 } } }, f(astnode3))
  end)
end)


describe("ex_parser.grammar", function()
  before_each(function()
    M.debug = false
    D.disable()
  end)

  it("pModuleName", function()
    local f = function(input) return M.p.module_name:match(input) end
    -- space befor for lpeg.B
    assert.same('Name', f(" Name"))
    assert.same('ModuleName', f(" ModuleName"))
    assert.same('ModuleName', f(" ModuleName,"))
    assert.same('ModuleName_', f(" ModuleName_,"))
    assert.same('Module.Name_', f(" Module.Name_,"))
    assert.same('Module', f(" Module,Name_,"))
    assert.is_nil(f(" aModule.Long_Name"))
    assert.same('Module.Long_Name', f(" Module.Long_Name"))
    assert.same('AST.Name', f(" AST.Name"))
    assert.same('AST.Name', f(" AST.Name"))
    assert.same('AST.Name', f("\n AST.Name"))
    assert.same('AST.Name', f("\n\t AST.Name"))
  end)

  it("pFuncName", function()
    local f = function(input) return M.p.func_name:match(input) end
    assert.is_nil(f("Name"))
    assert.same('_Name', f("_Name"))
    assert.same('_Func', f("_Func.Name")) -- ?
    assert.same('name', f("name"))
    assert.same('name?', f("name?"))
    assert.same('name!', f("name!"))
    assert.same('name', f("name,"))
  end)


  it("pDo", function()
    local f = function(input)
      return M.p._do:match(input)
    end
    assert.same(nil, f("do"))
    assert.same('do', f(" do"))
    assert.same('do', f("   do"))
    assert.same('do', f("\n do"))
    -- oneliner
    assert.same('do:', f(",do:"))
    assert.same('do:', f(" ,do:"))
    assert.same('do:', f(" , do:"))
    assert.same('do:', f(" , do: "))
  end)

  it("defmodule do-end multiline", function()
    local f = function(input) return M.p.defmodule:match(input) end
    local exp = { name = 'Name', tag = 'Module' }
    assert.same(exp, f("defmodule Name do end"))
    assert.same(exp, f(" defmodule Name do end"))
    assert.same(exp, f("\n defmodule Name do end"))
    assert.same(exp, f("\n  defmodule   Name   do   end  "))
    assert.same(exp, f("\n  defmodule   Name  \n do\n   end\n  "))
    assert.same(exp, f("\n  defmodule \n  Name  \n do\n   end\n  "))
    assert.is_nil(f("\n defmodule Bad Module Name do end"))
  end)

  it("defmodule ,do: -- oneliner", function()
    local f = function(input) return M.p.defmodule:match(input) end
    local exp = { name = 'Name', tag = 'Module' }
    assert.same(exp, f("\n defmodule Name, do: end"))
  end)

  it("pModuleUse", function()
    local f = function(input)
      local a, b = M.p.module_use:match(input)
      return tostring(a) .. '|' .. tostring(b)
    end
    assert.same('use|ExUnit.Case', f("use ExUnit.Case"))
    assert.same('use|ExUnit', f("use ExUnit"))
    assert.same('use|ExUnit', f(" use ExUnit"))
    assert.same('use|ExUnit', f(" use   ExUnit"))
    assert.same('use|ExUnit', f(" use   ExUnit  "))
  end)

  it("pModuleImport", function()
    local f = function(input)
      local a, b = M.p.module_import:match(input)
      return tostring(a) .. '|' .. tostring(b)
    end
    assert.same('import|ExUnit.Case', f("import ExUnit.Case"))
    assert.same('import|ExUnit', f("import ExUnit"))
    assert.same('import|ExUnit', f(" import ExUnit"))
    assert.same('import|ExUnit', f(" import ExUnit"))
    assert.same('import|ExUnit', f(" import   ExUnit  "))
  end)

  it("pDefFunc", function()
    local f = function(input) return M.p.deffunc:match(input) end
    local exp0 = { tag = 'Function', name = 'func_name' }
    assert.same(exp0, f("def func_name do end"))

    local exp = { tag = 'Function', name = 'func_name', private = true }
    assert.same(exp, f("defp func_name do end"))
  end)

  it("defmodule do-end multiline + funcs", function()
    local f = function(input) return M.p.defmodule:match(input) end

    local exp = {
      tag = "Module",
      name = "Name",
      import = { Kernel = true },
      use = { ["ExUnit.Case"] = true }
    }
    local res = f([[
    defmodule Name do
      use ExUnit.Case
      import Kernel
    end
]])
    assert.same(exp, res)
  end)

  it("defmodule do-end multiline + funcs", function()
    local f = function(input) return M.p.defmodule:match(input) end

    local exp = {
      tag = "Module",
      name = "Name",
      functions = {
        func_name = { tag = "Function", name = "func_name" },
        func_name2 = { tag = "Function", name = "func_name2" }
      }
    }
    local res = f([[
    defmodule Name do
      def func_name do
      end
      def func_name2 do
      end
    end
]])
    assert.same(exp, res)
  end)

  it("defmodule do-end multiline + use + import funcs", function()
    local f = function(input) return M.p.defmodule:match(input) end

    local exp = {
      tag = "Module",
      name = "Name",
      use = { ["ExUnit.Case"] = true },
      import = { Kernel = true },
      functions = {
        func_name = { tag = "Function", name = "func_name" },
        func_name2 = { tag = "Function", name = "func_name2" }
      }
    }
    local res = f([[
    defmodule Name do
      use ExUnit.Case
      import Kernel
      def func_name do
      end
      def func_name2 do
      end
    end
]])
    assert.same(exp, res)
  end)

  it("pAtoms", function()
    local f = function(input) return M.p.atom:match(input) end
    assert.same('Desk', f(' Desk')) -- Elixir.Desk IO.puts(Desk) --> Elixir.Desk
    assert.same('Desk!', f(' Desk!'))
    assert.same('Desk?', f(' Desk?'))
    assert.same('Desk', f(' Desk, '))
    assert.same('Desk', f(' Desk.Top'))
    assert.same(':Desk', f(' :Desk, '))
    assert.is_nil(f(' _Desk, '))
    assert.is_nil(f(' @Desk, '))
    assert.is_nil(f(' : Desk, '))
  end)
end)



describe("ex_parser.grammar", function()
  local value_rule_match = function(input)
    if M.debug and input and #input < 80 then
      local s = ''; for i = 1, #input do s = s .. tostring(i % 10) end
      print(s .. "\n" .. input)
    end
    local ret1, ret2 = M.p.valuerule:match(input)
    if ret2 ~= nil then
      error('has ret2:|' .. tostring(ret2) .. '|, ret1:|' .. tostring(ret1) .. '|')
    end
    return ret1
  end

  local f0 = value_rule_match
  -- match grammar rule
  local f = function(input, print_ast)
    local ast_node = value_rule_match(input)
    if print_ast then
      print("INPUT: |" .. tostring(input) .. '| AST:', inspect(ast_node))
    end
    local value = M.ast2value(ast_node)
    return value
  end

  it("pValueRule Atoms", function()
    -- Note: here used lpeg.B to ensure avoid case as '::atom'
    -- a side effect of this check is that the pattern will not work from the
    -- beginning of the string.
    -- Therefore, there is one space at the beginning everywhere
    assert.same(':atom', f(" :atom"))
    assert.same(':Atom', f(" :Atom"))
    assert.same(':a_to_m', f(" :a_to_m"))
    -- assert.same(':_a_to_m', f(" :_a_to_m")) --?
    assert.same(':atom0', f(" :atom0"))
    assert.is_nil(f(" :0atom"))
    assert.is_nil(f(" :@atom"))
    assert.is_nil(f(" :?_a_to_m"))
    assert.is_nil(f(" :._a_to_m"))
    assert.same(':ato', f(" :ato.m")) -- ?
    assert.same(':atom?', f(" :atom?"))
    assert.same(':atom!', f(" :atom!"))
    assert.same(':atom', f(" :atom,"))
    assert.same(':atom', f(" :atom+"))
    assert.same(':atom', f(" :atom:"))

    assert.same(':"atom"', f(' :"atom"'))
    assert.same(':"atom with spaces"', f(' :"atom with spaces"'))
    assert.same(':"atom\\n_name"', f(' :"atom\\n_name"'))
  end)

  it("pValueRule Numbers Dec & Hex", function()
    -- Note: space before num for lpeg.B
    -- Integer
    assert.same(0, f(" 0"))
    assert.same(16, f(" 0x10"))
    assert.same(255, f(" 0xFF"))
    assert.same(255, f(" 0xff"))
    --
    assert.same(1, f(" 1"))
    assert.same(-1, f(" -1"))
    assert.is_nil(f("- 1"))
    assert.same(-42, f(" -42"))
    assert.same(42, f(" 42"))

    assert.same(1000, f(" 1_000"))
    assert.same(1000000, f(" 1_000_000"))

    -- Float
    assert.same(0, f(" 0.0"))
    assert.same(1, f(" 1.0"))
    assert.same(4.2, f(" 4.2"))
    assert.same(420, f(" 4.2e2"))
    assert.same(42, f(" 42.0"))
    assert.same(-4.5, f(" -4.5"))
    assert.is_nil(f(" .0")) -- invalid syntax
  end)

  it("pValueRule Numbers Oct Bin(Integer)", function()
    assert.match_error(function()
      assert.same(15, f(" 0b1111"))
    end, 'bin Not implemented yet')

    assert.match_error(function()
      assert.same(15, f(" 0o777"))
    end, 'oct Not implemented yet')
  end)

  it("pValueRule String Oneliner", function()
    assert.same('', f('""'))
    assert.same('0', f('"0"'))
    assert.same('0', f(' "0"'))
    assert.is_nil(f(',"0"'))
    assert.same('0', f('"0",'))
    assert.same("\n", f('"\n"'))
    assert.same("'", f('"\'"'))
    assert.same('string', f('"string"'))
    assert.same('a b c', f('"a b c"'))
    assert.same("a\nb\nc", f('"a\nb\nc"'))
    assert.same("a\"b\nc", f('"a\\"b\nc"'))
    assert.same("a\"b\tc\rE", f('"a\\"b\tc\rE"'))
    -- not consume heredoc:
    assert.same('', f('"" "'))
    assert.is_nil(f('"""'))
  end)

  it('pValueRule String """ Heredoc', function()
    assert.is_nil(f('""""""'))
    assert.is_nil(f('""" """'))
    assert.same('', f('"""\n"""'))
    assert.same(' ', f('""" \n"""'))
    assert.is_nil(f('"""string"""'))
    assert.same('a b c', f('"""a b c\n"""'))
    assert.same('a b c"""', f('"""a b c"""\n"""'))
  end)


  it("pValueRule Tuple", function()
    assert.same({ value = {}, tag = 'Tuple' }, f0("{}"))
    assert.same({}, f("{}")) -- same with converting ast to value

    assert.same({ {} }, f("{{}}"))
    assert.same({ 1 }, f("{1}"))
    assert.same({ 1, 2 }, f("{1, 2 }"))
    assert.same({ 1, 2, 3 }, f("{1,2,3}"))
    assert.same({ 1, 2 }, f("{1 ,2 }"))
    assert.same({ 1, 2 }, f("{1, 2, }"))
    assert.same({ 1, 2 }, f("{1 ,2 ,}"))
    assert.same({ 1, 2, 3 }, f("{1, 2, 3}"))
    assert.same({ 1, { 2, { 3 } } }, f("{1, {2, {3},}, }"))
    -- syntax errors
    assert.is_nil(f("{,1}"))
    assert.is_nil(f("{1;}"))
    -- todo ?
    -- iex> {varname = 1} --> {1}
  end)

  it("pValueRule Simple List of Integers", function()
    assert.same({ tag = 'List', value = {} }, f0("[]"))
    assert.same({}, f("[]"))
    assert.same({ 1 }, f("[1]"))
    assert.same({ {} }, f("[[]]"))
    assert.same({ 1, 2 }, f("[1, 2]"))
    assert.same({ 1, 2, 3 }, f("[1, 2,3]"))
    assert.same({ 1, 2, 3 }, f("[1, 2,3,]"))
    assert.same({ 1, 2, 3, 4 }, f("[1, 2, 3, 4]"))
    assert.same({ 1, 2, 3, 4, 5, 6, 7, 8 }, f("[1, 2, 3, 4,5,6,7 ,8]"))
    assert.same({ 1 }, f("[1]"))
    assert.same({ 1 }, f("[ 1 ]"))
    assert.same({ 1, 2 }, f("[1,2]"))
    assert.same({ 1, 2 }, f("[1, 2]"))
    assert.same({ 1, {} }, f("[1,[]]"))
    assert.same({ 1, {} }, f("[1, [ ] ]"))
    assert.same({ 1, {} }, f("[1, [] ]"))
    assert.same({ 1, {} }, f("[1,[] ]"))
    assert.same({ 1, { 2 } }, f("[1,[2,] ]"))
    assert.same({ 1, { 2, 3 } }, f("[1,[2,3] ]"))
    assert.same({ 1, { 2, 3, {} } }, f("[1,[2,3,[]] ]"))
    assert.same({ 1, { 2, 3, {} } }, f("[1,[2,3,[]] ]"))
    -- syntax errors
    assert.is_nil(f("[,1, 2]"))
  end)

  it("pValueRule Simple List of Atoms", function()
    local exp = { { ':extra_applications', { ':logger' } } }
    assert.same(exp, f(" [ extra_applications: [:logger] ]"))
    assert.same(exp, f("[extra_applications: [:logger]]"))

    local exp2 = { { ':extra_applications', { ':logger' } }, { ':mod', 1 } }
    assert.same(exp2, f("[extra_applications: [:logger], mod: 1]"))

    local exp3 = { { ':extra_applications', { ':logger' } }, { ':mod', {} } }
    assert.same(exp3, f("[extra_applications: [:logger], mod: {}]"))

    local exp4 = {
      { ':extra_applications', { ':logger' } },
      { ':mod',                { ':atom', {} } }
    }
    assert.same(exp4, f("[extra_applications: [:logger], mod: {:atom, []}]"))
    -- local s = " [ extra_applications: [:logger], mod: {:Desk.Application, []} ]"
    -- bad syntax
    assert.same(nil, f("[extra_applications:[:logger]]"))
  end)

  it("pValueRule Simple List of Strings", function()
    assert.same({ 'a' }, f('[ "a" ]'))
    assert.same({ 'a' }, f('["a"]'))
    assert.same({ 'a', 'b' }, f('[ "a" , "b" ]'))
    assert.same({ 'a', 'b' }, f('["a","b"]'))
    assert.same({ 'a', 'b', 'c' }, f('[ "a" , "b" , "c" ]'))
    assert.same({ 'a', 'b', 'c' }, f('[ "a", "b", "c" ]'))
    assert.same({ 'a', 'bc', 'd e', 'f' }, f('["a", "bc", "d e", "f" ]'))
  end)

  it("pValueRule KeywordList", function()
    assert.same({ { ':a', 1 } }, f("[a: 1]"))
    assert.same({ { ':a', 1 }, { ':b', 2 } }, f("[a: 1, b: 2]"))
    assert.same({ { ':a', 1 }, { ':b', 2 } }, f("[a: 1,b: 2]"))
    assert.is_nil(f("[,a: 1, b: 2,]"))
    -- inner representation of kw-list (without syntax sugar)
    assert.same({ { ':a', 1 }, { ':b', 2 } }, f("[{:a, 1}, {:b, 2}]"))
    -- syntax error
    assert.is_nil(f("[a:1, b: 2]"))
    assert.is_nil(f("[a: 1,b:2]"))
    assert.is_nil(f("[a: :atom:]"))
    -- Atom
    assert.is_nil(f(" [A: 1]")) -- todo atom in KWList can starts with ^[AZ]
    assert.is_nil(f(" [A: :atom]"))
  end)

  it("pValueRule Map Sugar", function()
    assert.same({}, f("%{}"))
    assert.same({ [':a'] = 1 }, f("%{a: 1}")) -- iex> Map.keys(%{a: 1})

    assert.same({ [':a'] = 1, [':b'] = 2 }, f("%{a: 1, b: 2}"))
    assert.same({ [':a'] = 1, [':b'] = 2 }, f("%{a: 1,b: 2}"))
    assert.same({ [':a'] = 1, [':b'] = 2 }, f("%{a: 1,b: 2,}"))
    assert.same({ [':a'] = 1, [':b'] = {} }, f("%{a: 1,b: %{}}"))
    local exp = { [':a'] = 1, [':b'] = { [':c'] = 3 } }
    assert.same(exp, f("%{a: 1,b: %{c: 3}}"))

    -- syntax errors
    assert.is_nil(f("%{,a: 1,b: 2,}"))
    assert.is_nil(f("%{a:1}")) -- syntax error ': '
    assert.is_nil(f("%{a: }"))
  end)

  it("pValueRule Map FullSyntax %{:key => 8}", function()
    assert.same({}, f("%{}"))
    assert.same({ [':a'] = 1 }, f("%{:a=>1}"))
    assert.same({ [':a'] = 1 }, f("%{:a => 1}"))
    assert.same({ [':a'] = 1 }, f("%{:a=> 1}"))
    assert.same({ [':a'] = 1 }, f("%{:a =>1}"))
    assert.same({ [':a'] = 1 }, f("%{:a=>1}"))
    assert.same({ [':a'] = 1, [':b'] = 2, [':c'] = 3 }, f("%{:a=>1,:b=>2,:c=>3}"))
    assert.same({ [':a'] = 1, [':b'] = { [':c'] = 3 } }, f("%{:a=>1,:b=>%{:c=>3}}"))

    -- syntax error
    assert.is_nil(f("%{:a=>1,:b=>}"))
    assert.is_nil(f("%{:a => }"))
    assert.is_nil(f("%{,1=>2,3=>6,}"))
    assert.is_nil(f("%{1=>}"))
  end)

  it("pValueRule Map FullSyntax DiffTypes as Keys", function()
    -- Integer as Key
    assert.same({ [1] = 2, [3] = 6 }, f("%{1=>2,3=>6}"))
    assert.same({ [1] = 2, [3] = 6 }, f("%{1=>2,3=>6,}"))

    -- List as Key
    assert.same("{\n  [{}] = \":value\"\n}", inspect(f("%{[] => :value}")))
    assert.same("{\n  [{}] = {}\n}", inspect(f("%{[] => []}")))
    assert.same("{\n  [{}] = 8\n}", inspect(f("%{[] => 8}")))

    -- Tuple as Key
    assert.same("{\n  [{}] = \":value\"\n}", inspect(f("%{{} => :value}")))
    assert.same("{\n  [{}] = {}\n}", inspect(f("%{{} => []}")))
    assert.same("{\n  [{}] = 8\n}", inspect(f("%{{} => 8}")))

    -- Map as Key
    assert.same("{\n  [{}] = \":value\"\n}", inspect(f("%{%{} => :value}")))
    assert.same("{\n  [{}] = {}\n}", inspect(f("%{%{} => []}")))
    assert.same("{\n  [{}] = 8\n}", inspect(f("%{%{} => 8}")))

    assert.is_nil(f("%{false=>1}")) -- todo ?
    assert.same({ [':false'] = 1 }, f("%{:false=>1}"))
  end)

  it("pFuncCall", function()
    local f1 = function(input) return M.p.funccall:match(input) end
    local exp = {
      tag = 'Call',
      value = {
        module = 'Package.Module',
        func = 'funcname',
        params = {}
      }
    }
    assert.same(exp, f(' Package.Module.funcname()'))
    local exp2 = { tag = 'Call', value = { func = 'deps', params = {} } }
    assert.same(exp2, f1(' deps()'))
  end)

  it("pExpr", function()
    --
    local exp = {
      tag = 'ExprBin',
      value = {
        left = { tag = 'Call', value = { func = 'env', module = 'Mix', params = {} } },
        op = '==',
        right = ':prod',
      }
    }
    local res = f(' Mix.env() == :prod')
    assert.same(exp, res)
    local exp2 = { tag = 'Call', value = { func = 'deps', params = {} } }
    assert.same(exp2, f(' deps()'))
  end)


  local mixfile_no_comments = [==[
defmodule TaskDesk.MixProject do
  use Mix.Project

  def project do
    [
      app: :desk,
      version: "0.1.0",
      elixir: "~> 1.16",
    ]
  end

end
]==]

  it("defmodule with list ret value from func [no comments]", function()
    local match = function(input) return M.p.defmodule:match(input) end
    local exp = {
      tag = 'Module',
      name = 'TaskDesk.MixProject',
      use = { ['Mix.Project'] = true },
      functions = {
        project = {
          tag = 'Function',
          name = 'project',
          code = {
            tag = 'Code',
            value = {
              { -- list
                tag = "List",
                value = {
                  { tag = "Tuple", value = { ":app", ":desk" } },
                  { tag = "Tuple", value = { ":version", "0.1.0" } },
                  { tag = "Tuple", value = { ":elixir", "~> 1.16" } } }
              }
            }
          }
        }
      }
    }
    local res = match(mixfile_no_comments)
    assert.same(exp, res)
  end)

  it("pComment", function()
    local f2 = function(input) return M.p.comment:match(input) end
    local f1 = function(input)
      local ast = f2(input)
      M.assert_tag(ast, COMMENT)
      return ast[1] -- value?
    end
    -- TODO how to write comments into specified place?
    -- so that they are not inserted directly into data structures?
    assert.same('#', f1(" #"))
    assert.same('#', f1("  #"))
    assert.same('# comment', f1("  # comment"))
    assert.same('# comment', f1("  # comment\n code"))
    assert.same('#', f1("#"))
    assert.same('#', f1("#\n"))
    assert.same('# ', f1("# \n"))
    assert.same('# \\ ', f1("# \\ \n"))
  end)

  it("pFuncBody", function()
    local f1 = function(input)
      return { M.p.funcbody:match(input) }
    end
    assert.same({ { tag = 'Comment', '# comment 1' } }, f1("\n# comment 1"))

    local exp = {
      { tag = 'Comment', '# comment 1' },
      TAG_VALUE, -- used by foldFuncDef
      {
        tag = 'List',
        value = { 1 }
      }
    }
    assert.same(exp, f1("\n# comment 1\n[1]"))
  end)

  local mixfile_with_comments = [==[
defmodule TaskDesk.MixProject do
  use Mix.Project

  def project do
    # comment 1
    # comment 2
  end

end
]==]

  it("defmodule with list ret value from func [with comments]", function()
    local f1 = function(input) return M.p.defmodule:match(input) end
    local exp = {
      tag = 'Module',
      name = 'TaskDesk.MixProject',
      use = { ['Mix.Project'] = true },
      functions = { project = { tag = 'Function', name = 'project' } }
    }

    M.comments = true
    local res = f1(mixfile_with_comments)
    assert.same(exp, res)
  end)

  -- example how to debug lpeg patterns with visualizing
  --[[
  it("dvisualize", function()
    local p = M.I('A') * lpeg.P('word') * M.I('B')
    D.enable_module(M)
    assert.same(5, lpeg.match(p, 'word'))
  end)
]]
end)
