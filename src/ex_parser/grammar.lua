-- 20-03-2024 @author Swarg
-- Grammars Syntax rules for Elixir lang
-- Goal: parse simlple exs-config files into json-like representation
--

local lpeg = require 'lpeg'
local D = require 'dprint'

local loc = lpeg.locale()
local P = lpeg.P
-- lpeg.B For making sure that a match is not preceded by a given pattern
local B = lpeg.B
local R = lpeg.R
local C = lpeg.C
local V = lpeg.V
local Ct = lpeg.Ct
local Cc = lpeg.Cc
local Cg = lpeg.Cg
local Cf = lpeg.Cf
local lpegS = lpeg.S

local M = {}
M._VERSION = 'ex_parser 0.1.0'

local dprint = D.mk_dprint_for(M, false)
local dvisualize = D.mk_dvisualize_for(M, false)
M.devmode = false -- check interpreter


local E = {}
local ESC = "\\"
local S = loc.space ^ 0
local S1 = loc.space ^ 1

local M_any = P(1)
local pQuote = P('"')
local pNewline = P('\r') ^ -1 * '\n'
local pNonnewline = 1 - pNewline
local pNonnewline_esc = 1 - (pNewline + ESC) + ESC * M_any

local pOSB = lpeg.P("[") -- opening square bracket
local pCSB = lpeg.P("]")

local pOCB = lpeg.P("{") -- opening curly bracket
local pCCB = lpeg.P("}")

local pOPB = lpeg.P("(")
local pCPB = lpeg.P(")")


local pAlpha = loc.alpha
local pAlnum = loc.alnum
local pUpper = loc.upper
local pLower = loc.lower
local pDot = lpeg.P(".")
local pComma = S * lpeg.P(",")
local pUnderscore = lpeg.P("_")
local pSpaceSep = loc.space ^ 1
-- ,do: oneliner  do-end
local pDo = (S * pComma * S * lpeg.P("do") * S * lpeg.P(":") * lpeg.Cc("do:")) +
    (pSpaceSep * lpeg.C("do"))

local pEnd = S * lpeg.P("end")


local TAG_FILE        = 'SourceFile' -- SourceFile
local TAG_MODULE      = 'Module'
local TAG_FUNCTION    = 'Function'
local TAG_COMMENT     = 'Comment'
local TAG_CODE        = 'Code'
local TAG_CALL_FUNC   = 'Call'
local TAG_EXPR_BINARY = 'ExprBin'   -- left op right
local TAG_EXPR_UNARY  = 'ExprUnary' -- op right
local TAG_VALUE       = 'Value'     --
local TAG_TUPLE       = 'Tuple'
local TAG_LIST        = 'List'
local TAG_MAP         = 'Map'
-- Integer Float String Atom Sigils ??


local function get_tag(el)
  return type(el) == 'table' and el.tag or nil
end

--
-- throws an exception if the tags do not match
--
---@param el any      table{tag, value}
---@param tag string  expected tag
---@param msg string?
function M.assert_tag(el, tag, msg)
  local at = type(el)
  if msg then msg = ' ' .. tostring(msg) else msg = '' end
  local ts = tostring
  if at == 'table' then
    if el.tag == tag then
      return el.tag --             << OK
    end
    error('expected tag == ' .. ts(tag) .. ' got: ' .. ts(el.tag) .. msg)
  end
  error('expected table got: ' .. ts(at) .. msg)
end

local assert_tag = M.assert_tag

--
--------------------------------------------------------------------------------
--                              Debugging
--------------------------------------------------------------------------------


local I = function(tag)
  return lpeg.P(function(s, i)
    dvisualize(s, i, i, 'TAG:', tag)
    return true
  end)
end
---@diagnostic disable-next-line: unused-local
local I2 = lpeg.P(function(s, i)
  dvisualize(s, i, i)
  return true
end)

local function debug_show_acc(acc)
  if D.is_enabled(M) then
    local inspect = require "inspect"
    print('ACC:', inspect(acc))
    print("-----------------------------------------------------------------\n")
  end
end


--------------------------------------------------------------------------------

function M.foldComment(v)
  return { tag = TAG_COMMENT, v } -- value?
end

function M.foldTuple(data)
  local tuple = { tag = TAG_TUPLE, value = data }
  return tuple
end

function M.foldList(data)
  local list = { tag = TAG_LIST, value = data }
  return list
end

-- atom -> :atom
-- Desk -> :Elixir.Desk ?
---@param pair table
function M.foldKWPair(pair)
  assert(type(pair) == 'table', 'kwpair')

  local key = pair[1]
  local fc = key:sub(1, 1)
  if fc ~= ':' then
    if string.upper(fc) == fc then
      -- pair[1] = 'Elixir'.. pair[1] -- todo?
    else
      pair[1] = ':' .. pair[1] -- atom -> :atom
    end
  end

  local tuple = { tag = TAG_TUPLE, value = pair }
  -- todo ? Desk -> ..
  return tuple
end

---@param mpairs table{ {key, value}, }
function M.foldMap(mpairs)
  assert(type(mpairs) == 'table', 'mpairs')
  local map = { tag = TAG_MAP, value = {} }

  for _, pair in ipairs(mpairs) do
    local tag = get_tag(pair)
    local k, v
    if tag == TAG_TUPLE then -- Syntax Sugar for Atoms(like KWList) %{k: 8}
      k, v = (pair or E).value[1], (pair or E).value[2]
    else
      k, v = pair[1], pair[2] -- %{ key => value}
    end
    if k == nil then
      print("[DEBUG] mpairs:", require "inspect" (mpairs))
      error('no key')
    end
    map.value[k] = v
  end
  return map
end

---@param s string
function M.foldToNumber(s)
  if type(s) == 'string' then
    local c = s:sub(1, 2)
    if c == '0b' then
      error('bin Not implemented yet ' .. tostring(s))
    elseif c == '0o' then
      error('oct Not implemented yet ' .. tostring(s))
    elseif string.find(s, '_') then
      s = string.gsub(s, '_', '')
    end
  end
  return tonumber(s)
end

function M.foldBinExpr(left, op, right)
  return {
    tag = TAG_EXPR_BINARY,
    value = { left = left, op = op, right = right }
  }
end

-- funcname, params
-- fullmodulename, funcname, params
---@diagnostic disable-next-line: unused-local
function M.foldFuncCall(...)
  local argscnt = select('#', ...)
  local call = { tag = TAG_CALL_FUNC, value = {} }
  local v = call.value
  if argscnt == 3 then
    v.module = select(1, ...)
    v.func = select(2, ...)
    v.params = select(3, ...) -- {}
  elseif argscnt == 2 then
    v.func = select(1, ...)
    v.params = select(2, ...)
  else
    error('Unexpected args count' .. tostring(argscnt))
  end
  return call
end

function M.foldFuncDef(acc, a1, a2)
  acc = acc or {}

  local a2_type = type(a2)
  dprint('ctx:', acc.tag, 'a1:', ':dump:', a1, 'a2:', ':dump:', a2)

  -- if a1 == 'do' ??

  if a2_type == 'string' and (a1 == 'def' or a1 == 'defp') then
    acc.tag = TAG_FUNCTION
    acc.name = a2
    if a1 == 'defp' then acc.private = true end
    --
  elseif a1 == TAG_VALUE and a2 then
    acc.code = acc.code or { tag = TAG_CODE, value = {} }
    acc.code.value[#acc.code.value + 1] = a2
    -- on enddef foldFunction must add acc.return_value from acc.code[#acc.code]
  end

  debug_show_acc(acc)
  return acc
end

---@param acc string|table body of one module
---@return table
function M.foldModule(acc, a1, a2)
  acc = acc or {}
  ---@cast acc table

  local tag, a2_type = get_tag(a1), type(a2)
  dprint('ctx:', acc.tag, 'a1:', ':dump:', a1, 'a2:', ':dump:', a2)

  if tag == TAG_COMMENT then -- in any ctx
    a1 = a1                  -- dprint(TAG_COMMENT, a1[2]) +
    -- TODO save with linenumbers if configured
    --
  elseif tag == TAG_FUNCTION then
    local funcbody = a1
    acc.functions = acc.functions or {}
    acc.functions[assert(funcbody.name, 'funcname')] = funcbody
    --
  elseif a1 == 'defmodule' and a2_type == 'string' then -- a3 == do
    dprint("defmodule", a2)
    acc.tag = TAG_MODULE
    acc.name = a2
    acc.moduledoc = nil -- todo
    --
  elseif a1 == 'use' and a2_type == 'string' then
    acc.use = acc.use or {}
    acc.use[a2] = true -- todo use opts
    --
  elseif a1 == 'import' and a2_type == 'string' then
    acc.import = acc.import or {}
    acc.import[a2] = true
    --
  end

  debug_show_acc(acc)
  return acc
end

--
--
---@return table
function M.foldSourceFile(acc, el)
  acc = acc or { tag = TAG_FILE }

  local tag = get_tag(el)
  dprint('ctx:', acc.tag, 'el:', tag, ':dump:', el)

  if tag == TAG_COMMENT then
    acc.comments = acc.comments or {}
    acc.comments[#acc.comments + 1] = el[1] -- ?
    --
  elseif tag == TAG_MODULE then
    acc.modules = acc.modules or {}
    acc.modules[assert(el.name, 'module name')] = el
  else
    dprint('Unexpected element', ':dump:', el)
    error('[DEV] Stop Parsing!')
    acc[#acc + 1] = el -- ? TODO code outside the module
  end

  return acc
end

--------------------------------------------------------------------------------


-- %s*[AZ][azAZ09._]*
-- local pModuleName = S * lpeg.C(pUpper ^ 1 * (pAlnum + pDot + pUnderscore) ^ 0)
-- %s*[az_][azAZ09_]*[!?]?
local pFuncName = S * lpeg.C((pLower + pUnderscore) ^ 1 *
  (pAlnum + pUnderscore) ^ 0 * (lpeg.S("!?")) ^ -1) -- ? * #S1


local pComment = S * ((P('#') * pNonnewline_esc ^ 0) / M.foldComment)
-- any number even none
local pComments = pComment ^ 0


-- Atoms.
-- lpeg.B used to ensure that before no ':'-prefix to avoid '::atom'
-- hence these patterns will not pass from the very beginning in the string
-- :"atom name"
local pAtom1 = B(1 - P(':')) * P(':"') * (P(1) - P('"')) ^ 0 * P('"') -- / M.foldAtomInQuote
-- :atom :atom? :atom! :at@m :atom_too
local pAtom2 = B(1 - P(':')) * ':' * pAlpha * (pAlnum + lpegS('_@')) ^ 0 * lpegS('?!') ^ -1
-- AtomName --> Elixir.AtomName (for modulenames try: iex> IO.puts(Atom) is_atom(Atom))
-- PartOfModuleName
local pAtom3 = B(1 - (pAlnum + lpegS('_:'))) * pUpper * (pAlnum + lpegS('_@')) ^ 0 * lpegS('?!') ^ -1
local pAtom = S * C(pAtom1 + pAtom2 + pAtom3)
-- Notes:
-- :"atom name with space" --> ':"atom name with space"' ??
local pModuleName = S * C(pAtom3 * (pDot * pAtom3) ^ 0)

local pModuleUse = S * lpeg.Cg(lpeg.C("use") * S1 * pModuleName)
local pModuleImport = S * lpeg.Cg(lpeg.C("import") * S1 * pModuleName)

-- atom in KWList
local pKwAtom = S * C(R("az", "AZ") * (pAlnum + pDot + pUnderscore) ^ 0)
-- local pKwAtom = S * B(1 - (pAlnum + lpegS('_:'))) * -- TODO ^[AZ]
--     C(R("az", "AZ") * (pAlnum + pDot + pUnderscore) ^ 0)

-- Numbers
local pNumBin = '0b' * lpegS('01') ^ 1
local pNumOct = '0o' * R('07') ^ 1
local pNumHex = '0x' * R('09', 'AF', 'af') ^ 1
local pNumDec = R('09') * (R('09') + '_') ^ 0
local pInteger = pNumBin + pNumHex + pNumOct + pNumDec

local pFloat = loc.digit ^ 1 * '.' * loc.digit ^ 1
    * (lpegS('eE') * lpegS('+-') ^ -1 * loc.digit ^ 1) ^ -1

local pNumber = S * C(B(1 - (loc.alpha + '_')) *
  lpegS('+-') ^ -1 * (pFloat + pInteger)) / M.foldToNumber



-- Strings
local escapes_t = { n = "\n", t = "\t", r = '\r', ['"'] = '"', [ESC] = ESC }
local pEscapes = lpegS('ntr') + ESC + pQuote
local pMagic = ESC + pQuote
local pCC = P(1) - pMagic -- common chars
local pSContent = (pCC + ESC * lpeg.C(pEscapes) / escapes_t) ^ 0
local pStringS = S * pQuote * lpeg.Cs(pSContent) * pQuote * #(P(1) - '"' + P(-1))
-- Note: in the end ensure it is not a heredoc string """
-- meaning - any chars except " or endofline(-1)
-- Heredoc string
local pSContentM = (-P('\n"""') * (P(1) - ESC) + ESC * C(pEscapes) / escapes_t) ^ 0
local pStringM = S * '"""' * lpeg.C(pSContentM) * pNewline * '"""'

local pString = pStringM + pStringS

-- TODO keywors for pVariable
local exmacro = {
  'defstruct', 'defrecordp', 'defrecord', 'defprotocol', 'defp', 'defoverridable',
  'defmodule', 'defmacrop', 'defmacro', 'defimpl', 'defexception', 'defdelegate',
  'defcallback', 'def'
}
local keywords = {
  'is_atom', 'is_binary', 'is_bitstring', 'is_boolean', 'is_float', 'is_function',
  'is_integer', 'is_list', 'is_map', 'is_number', 'is_pid', 'is_port', 'is_record',
  'is_reference', 'is_tuple', 'is_exception', 'case', 'when', 'cond', 'for', 'if',
  'unless', 'try', 'receive', 'send', 'exit', 'raise', 'throw', 'after', 'rescue',
  'catch', 'else', 'do', 'end', 'quote', 'unquote', 'super', 'import', 'require',
  'alias', 'use', 'self', 'with', 'fn'
}

local pReservedWords = lpeg.P(false)
for _, w in ipairs(exmacro) do pReservedWords = pReservedWords + w end
for _, w in ipairs(keywords) do pReservedWords = pReservedWords + w end
pReservedWords = pReservedWords * -loc.alnum

-- Operators
local pBoolOps = (P('and') + 'or' + 'not' + 'when' + 'xor' + 'in') * -pAlnum
local pOperators = P('!==') + '!=' + '!' + '=~' + '===' + '==' + '=' +
    '<<<' + '<<' + '<=' + '<-' + '<' + '>>>' + '>>' + '>=' + '>' + '->' +
    '--' + '-' + '++' + '+' + '&&&' + '&&' + '&' + '|||' + '||' + '|>' + '|' +
    '..' --[[+ '.']] + '^^^' + '^' + '\\\\' + '::' + '*' + '/' + '~~~' + '@'

local pOperatorsAll = S * C(pBoolOps + pOperators)

-- aliases for use inside grammar rules
local pTuple = lpeg.V("tuple")
local pList = lpeg.V("list")
local pMap = lpeg.V("map")

local pValue = lpeg.V("value")
local pSimpleValue = lpeg.V("simple_value")
local pExpr = lpeg.V("expr")
local pLeftExpr = lpeg.V("expr_left")
local pRightExpr = lpeg.V("expr_right")

local pVariable = lpeg.P(false) -- TODO not a kwords

-- functions
local pFuncParams = S * Ct(S) -- TODO p[Simple?]Value move to grammar rules

-- Package.Method.funcname(params) or func(params)
local pFuncCall = (
  Cg((pModuleName + pDot) ^ 0 * pFuncName * pOPB * pFuncParams * pCPB)
  / M.foldFuncCall
)


-- grammar rules
local pValueRule = lpeg.P { "value",
  -- simple_value = pModuleName + pAtom + pFloat + pInteger + pString + pTuple +
  simple_value = pModuleName + pAtom + pNumber + pString + pTuple +
      pList + pMap,

  value = (pExpr + V('simple_value')),

  tuple = S * -P("%") * Ct(pOCB * S *
    -- value [(,value)|,]
    (pValue * (pComma * pValue + pComma) ^ 0) ^ -1
    * S * pCCB
  ) / M.foldTuple,

  kwpair = lpeg.Ct(pKwAtom * P(": ") * pValue) / M.foldKWPair,

  list_elm = (V("value") + V("kwpair")),
  -- list_exp = (V("list_elm") * (pComma * V("list_elm") + pComma) ^ 0) ^ -1,
  list_exp = (pComments * V("list_elm") *
    (pComma * pComments * V("list_elm") + pComments * pComma * pComments) ^ 0) ^ -1,

  list = S * pOSB * S * Ct(lpeg.V("list_exp")) / M.foldList * S * pCSB, -- []

  mpair = lpeg.Ct(V("value") * S * P("=>") * S * V("value")),

  map = S * lpeg.Ct(P("%{") * S *
    ((V("kwpair") + V("mpair")) * (pComma * (V("kwpair") + V("mpair")) + pComma) ^ 0) ^ -1
    * S * P("}")) / M.foldMap,

  -- Expressions
  -- TODO instead simple value here must be a destructor via pattern-matching
  expr_left = S * (pFuncCall + pSimpleValue + pVariable),
  expr_right = S * (pFuncCall + pValue),
  -- :EXPR: - something like a marker so that you can determine that
  -- this table is not a value but an expression that still needs to be calculated
  expr_binary = Cg(pLeftExpr * S * pOperatorsAll * pRightExpr) / M.foldBinExpr,
  -- todo unary like -var
  expr = V('expr_binary') + pFuncCall,
}


-- Function Body
-- local pFuncBody = S * Ct((pComment ^ 0) * (pValueRule ^ 0) * (pComment ^ 0))
local pFuncBody = S * (pComment ^ 0) *
    (Cg((Cc(TAG_VALUE) * pValueRule)) ^ 0) * -- ???
    (pComment ^ 0)

--
local pDefineFunc = Cf(Cc(nil) * -- init new parsing (init acc value)
  S * lpeg.Cg(lpeg.C(lpeg.P("defp") + lpeg.P("def")) * S1 * pFuncName) * pDo *
  pFuncBody *
  pEnd, -- * lpeg.Cc("endfunc"),
  -- todo ,do: oneliner
  M.foldFuncDef
)


-- mix.exs like files
local pDefmodule = Cf(
  Cc(nil) * -- cannot just use an Cc({}) here
  S * Cg(lpeg.C("defmodule") * S1 * pModuleName * pDo) *
  -- todo moduledoc
  pComments *
  (pModuleUse + pModuleImport) ^ 0 *
  -- todo doc
  (pComments * pDefineFunc * pComments) ^ 0 *
  pComments *
  pEnd
  ,
  M.foldModule
)

local pSourceFile = Cf(
  Cc(nil) * -- cannot use here Cc({})
  pComments *
  -- todo code before defmodule
  (pDefmodule ^ 0) *
  -- todo code after defmodule
  pComments
  ,
  M.foldSourceFile
)


-- testing
local p = {}
p.sourcefile = pSourceFile
p.defmodule = pDefmodule
p.module_name = pModuleName
p.func_name = pFuncName
p._do = pDo -- do-end
p.module_use = pModuleUse
p.module_import = pModuleImport
p.deffunc = pDefineFunc
p.funcbody = pFuncBody
p.funccall = pFuncCall
p.valuerule = pValueRule
p.atom = pAtom
p.tuple = pTuple
p.list = pList
p.map = pMap
p.stringM = pStringM
p.comment = pComment
p.nonnewline = pNonnewline
p.nonnewline_esc = pNonnewline_esc
p.expr = pExpr
p.exmacro = exmacro
p.keywords = keywords
p.reserved_words = pReservedWords
M.p = p
M.I = I
M.I2 = I2
M.dprint = dprint

M.tags = {
  file = TAG_FILE,
  module = TAG_MODULE,
  func = TAG_FUNCTION,
  exprbin = TAG_EXPR_BINARY,
  code = TAG_CODE,
  call = TAG_CALL_FUNC,
  comment = TAG_COMMENT,
  value = TAG_VALUE,
  tuple = TAG_TUPLE,
  list = TAG_LIST,
  map = TAG_MAP,
}

--------------------------------------------------------------------------------
--                               PARSER
--------------------------------------------------------------------------------

--
-- parse exs-config source code to AST-like structure
--
---@param input string
---@param opts table?
function M.parse(input, opts)
  assert(type(input) == 'string' and #input > 0,
    'expected not empty string as input got: ' .. tostring(input))

  opts = opts or {}

  local tree = lpeg.match(pSourceFile, input)

  return tree
end

-------------------------------------------------------------------------------

---@return table{root, modulename, interpreter}
function M.validate_ctx(ctx)
  assert(type(ctx) == 'table', 'ctx got: ' .. tostring(ctx))
  assert(type(ctx.root) == 'table', 'ctx.root got: ' .. tostring(ctx.root))

  assert(ctx.root.tag == TAG_FILE,
    'ctx.root.tag SourceFile got: ' .. tostring(ctx.root.tag))

  assert(type(ctx.modulename) == 'string',
    'ctx.modulename got: ' .. tostring(ctx.modulename))

  assert(type(ctx.interpreter) == 'function',
    'ctx.interpreter got: ' .. tostring(ctx.interpreter))

  return ctx
end

local validate_ctx = M.validate_ctx

--
-- extract data structure from AbstractSyntax[!SUB!]Tree
--
---@param node table
---@param ctx table?{root, modulename, interpreter}
function M.ast2value(node, ctx)
  local atype = type(node)

  local value = nil --?
  local tag = get_tag(node)
  dprint('ast2value:', atype, 'tag:', tag, ':dump:', node)

  -- if atype == 'table' and node.tag and node.value then
  if tag and node.value then
    dprint('ast2value tag:', tag, 'has value')
    if tag == TAG_TUPLE or tag == TAG_LIST then
      value = {}
      for _, v in ipairs(node.value) do
        local v0 = M.ast2value(v, ctx)
        if v0 ~= nil and get_tag(v0) ~= TAG_COMMENT then
          value[#value + 1] = v0
        end
      end
    elseif tag == TAG_MAP then
      value = {}
      for k, v in pairs(node.value) do
        local k0 = M.ast2value(k, ctx)
        local v0 = M.ast2value(v, ctx)
        -- todo comments
        if k0 ~= nil and v0 ~= nil then
          value[k0] = v0
        end
      end
      --
    elseif tag == TAG_COMMENT then
      return nil -- NOT_A_VALUE
      --
    elseif tag == TAG_CALL_FUNC then
      dprint('FuncCall', (ctx or E).modulename)
      if ctx and type(ctx.interpreter) == 'function' then
        return ctx.interpreter(node, ctx)
      else
        if M.devmode then
          error('no interpreter!') -- for dev
        end
        return node                -- unchanged!
      end
      --
    elseif tag == TAG_EXPR_BINARY then
      dprint('BinExpr has ctx:', ctx ~= nil)
      if ctx then
        assert(node.value.op)
        local right = M.ast2value(node.value.right, ctx)
        local left = M.ast2value(node.value.left, ctx)
        value = M.operate(ctx, left, node.value.op, right)
      else
        value = node
      end
      --
    elseif tag == TAG_EXPR_UNARY then
      error('Not implemented yet')
    elseif tag == TAG_CODE then
      error('Not implemented yet')
    else
      error('Unexpected tag: ' .. tostring(tag))
    end
  else
    value = node
  end

  dprint('return', ':dump:', value)
  return value
end

--------------------------------------------------------------------------------

-- evaluate function call
-- ast - list of code params - params to call
--
---@param ast table
---@param params table
---@param ctx table?{root, modulename, interpreter}
---@return any
function M.evaluate(ast, params, ctx)
  local tag = get_tag(ast)
  dprint("evaluate", 'tag:', tag, ':dump:', ast)
  ctx = validate_ctx(ctx)

  if tag == TAG_FUNCTION then
    return M.evaluate((ast.code or E).value, params, ctx)
  end

  assert(type(ast) == 'table', 'ast got: ' .. tostring(ast))
  assert(type(params) == 'table', 'params got: ' .. tostring(params))

  local last = assert(ast[#ast], 'last expression from code-block')

  dprint("last:", ':dump:', last, 'has-root:', ctx.root ~= nil, ctx.modulename)
  local value = M.ast2value(last, ctx)

  dprint("evaluated value:", ':dump:', value)

  return value
end

---@param ctx table{root, modulename, interpreter}
---@param op string
function M.operate(ctx, left, op, right)
  dprint('operate', ':dump:', left, op, ':dump:', right)
  ctx = validate_ctx(ctx)
  if op == '==' then
    return left == right
  else
    error('Not implemented yet op: ' .. tostring(op))
  end

  -- return { left, op, right }
end

---@param astnode table
---@param ctx table?{root, modulename, interpreter}
function M.simple_interpreter(astnode, ctx)
  dprint("simple_interpreter")
  ctx = validate_ctx(ctx)

  assert(type(astnode) == 'table', 'astnode')
  assert_tag(ctx.root, TAG_FILE)
  local tag = get_tag(astnode)

  if tag == TAG_CALL_FUNC then
    assert(astnode.value, 'Call with value')
    local modname = assert(astnode.value.module or ctx.modulename, 'module name')
    local funcname = assert(astnode.value.func, 'function name')
    local params = {} -- todo
    local func_sub_ast = M.get_function_sub_ast(ctx.root, modname, funcname)
    assert(func_sub_ast, 'should find sub ast for ' ..
      tostring(modname) .. '.' .. tostring(funcname)
    )
    local value = M.evaluate(func_sub_ast, params, ctx)
    return value
  end

  error('Not implemented yet' .. tostring(tag))
  return nil
end

--
-- fetch a sub-ast of a a given function-name in the given module-name
--
---@param ast table        - root of the AST with TAG_FILE
---@param modname string
---@param funcname string
function M.get_function_sub_ast(ast, modname, funcname)
  local tag = get_tag(ast)
  dprint("get_function_sub_ast", tag, modname, funcname)
  local ts = tostring
  assert(ast.tag == TAG_FILE, 'SourceFile expected got: ' .. ts(tag))
  local target = tostring(modname) .. '.' .. tostring(funcname)

  local mod = assert((ast.modules or E)[modname], 'module ' .. target)
  -- dprint('mod:', ':dump:', mod)
  local func = assert((mod.functions or E)[funcname], 'function ' .. target)

  return func
end

---@param ctx table?{root, modulename, interpreter}
function M.addMixStub(ctx)
  ctx = validate_ctx(ctx)

  -- local mixmod = ctx.root.modules['Mix'] or {}
  local mixmod = {
    name = 'Mix',
    tag = TAG_MODULE,
    functions = {
      env = {
        tag = TAG_FUNCTION,
        name = 'Mix',

        code = {
          tag = 'Code',
          value = {
            ':false'
          }
        }
      }
    }
  }
  ctx.root.modules['Mix'] = mixmod
  return ctx -- deep copy?
end

--
-- Convert AST of mix.exs to a Json-like(luatable) data structure
--
-- calls and calculates the values of all public functions in all modules
-- of the source file. which had previously been parsed into the AST
--
-- Goal: convert mix.exs into luatable config representation
--
---@param ast table
---@param ctx table?{root, modulename, interpreter}
---@return table|any
function M.to_obj(ast, ctx)
  assert(type(ast) == 'table', 'ast got:' .. tostring(ast))
  ctx = validate_ctx(ctx)

  local t
  local tag = get_tag(ast)

  dprint('to_obj tag:', tag)
  if tag == TAG_FILE then
    t = {}
    for name, body in pairs(ast.modules) do
      t[name] = M.to_obj(body, ctx)
    end
    --
  elseif tag == TAG_MODULE then
    t = {}
    for name, body in pairs(ast.functions) do
      if not body.private then
        t[name] = M.to_obj(body, ctx)
      end
    end
    --
  elseif tag == TAG_FUNCTION then
    return M.to_obj(ast.code, ctx)
    --
  elseif tag == TAG_CODE then
    local params = {}
    local value = M.evaluate(ast.value, params, ctx)
    dprint('evaluated code ', ctx.modulename, 'ret value:', ':dump:', value)
    return value
    --
  else
    error('Not implemented yet for tag:' .. tostring(tag))
  end

  return t
end

--
--
-- Usage Example:
--
--   local ast = ex_parser.parse(content)
--   local ctx = ex_parser.mk_ctx(ast, true)
--   local obj = ex_parser.to_obj(ast, ctx)
--
---@param ast table
---@param ismix boolean?
---@param modulename string?
---@param interpreter function?
function M.mk_ctx(ast, ismix, modulename, interpreter)
  assert(type(ast) == 'table' and ast.tag == TAG_FILE, 'ast got: ' .. tostring(ast))

  local firstmodname = assert(next(ast.modules), 'expected at least one module')

  local ctx = {
    root = ast,
    modulename = modulename or firstmodname,
    interpreter = interpreter or M.simple_interpreter
  }
  if ismix then -- use Mix.Project ??
    ctx = M.addMixStub(ctx)
  end
  return ctx
end

return M
