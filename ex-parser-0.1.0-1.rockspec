---@diagnostic disable: lowercase-global
package = "ex-parser"
version = "0.1.0-1"

source = {
  url = "git://gitlab.com/lua_rocks/ex-parser.git",
  -- tag = 'v0.1.0',
}

description = {
  summary = "Elixir programming language Parser. (AST & convert to luatable)",
  detailed = [[
  This is a library designed for parsing elixir code into an AST with the
ability to convert it into a JSON-like structure(Nested LuaTable).
Written in LPeg. Can evaluate simple code within a single source file to
produce json-like config representation of exs-files (mix.exs).
  Primary Goal: convert mix.exs-like files into luatable config representation
]],
  homepage = "https://gitlab.com/lua_rocks/ex-parser",
  maintainer = 'Swarg',
  license = "MIT"
}

dependencies = {
  'lua >= 5.1',
  'lpeg ~> 1.1',
}

build = {
  type = "builtin",
  modules = {
    ["ex_parser.grammar"] = "src/ex_parser/grammar.lua"
  }
}
