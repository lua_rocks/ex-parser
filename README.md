## Elixir Parser

  This is a library designed for parsing elixir code into an AST with the
ability to convert it into a JSON-like structure(Nested LuaTable).
Written in LPeg. Can evaluate simple code within a single source file to
produce json-like config representation of an exs-files (mix.exs).


## How evaluation of source code into an object works.

- when evaluating function bodies, evaluates only the last expression and
  returns its value as the function value

## Primary Goal

Convert mix.exs-like files into luatable config representation.

- Early in development
- based od LPeg


## Usage example

```lua
local parser = require 'ex_parser.grammar'

local content
-- read file content
local file = io.open(path, "rb")
if file then
  content = file:read "*a"
  file:close()
end

local ast = ex_parser.parse(content)
local ctx = ex_parser.mk_ctx(ast, true)
local obj = ex_parser.to_obj(ast, ctx)

```

## TODO
